package com.example.gojek_apk;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class OrderActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstancestate) {

        //TOdo auto generate method stub
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_order);
        final EditText name =(EditText) findViewById(R.id.edt1);
        final EditText alamat =(EditText) findViewById(R.id.edt2);
        final EditText pesanan =(EditText) findViewById(R.id.edt3);
        Button button =(Button) findViewById(R.id.btnorder);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(OrderActivity.this,DataActivity.class);
                i.putExtra("name",name.getText().toString());
                i.putExtra("alamat",alamat.getText().toString());
                i.putExtra("pesanan",pesanan.getText().toString());
                startActivity(i);

            }
        });


    }
}
