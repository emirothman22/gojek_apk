package com.example.gojek_apk;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity  extends AppCompatActivity implements View.OnClickListener {

    ImageButton imagebtn;

    @Override
    protected void onCreate(Bundle savedInstancestate) {

        //TOdo auto generate method stub
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_home);

        imagebtn = (ImageButton) findViewById(R.id.makanan);
        imagebtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.makanan:
                Intent explicit = new Intent(HomeActivity.this, OrderActivity.class);
                startActivity(explicit);
                break;
            default:
                break;
        }
    }
}