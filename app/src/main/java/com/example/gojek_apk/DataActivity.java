package com.example.gojek_apk;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DataActivity extends AppCompatActivity implements View.OnClickListener {
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstancestate) {

        //TOdo auto generate method stub
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_data);

        cancel = (Button) findViewById(R.id.btncancel);
        cancel.setOnClickListener(this);
        TextView n = (TextView) findViewById(R.id.textnama);
        TextView a = (TextView) findViewById(R.id.textalamat);
        TextView o = (TextView) findViewById(R.id.textorder);
        Bundle bundle = getIntent().getExtras();
        String s = bundle.getString("name");
        String t = bundle.getString("alamat");
        String u = bundle.getString("pesanan");

        n.setText(s);
        a.setText(t);
        o.setText(u);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btncancel:
                Intent explicit = new Intent(DataActivity.this, OrderActivity.class);
                startActivity(explicit);
                break;
            default:
                break;
        }





    }
}
